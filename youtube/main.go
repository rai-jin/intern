package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"mime/multipart"
	"net/http"
	"os"
	"strconv"
)

const (
	VIDEO_URL = "https://youtube.googleapis.com/youtube/v3/videos"
)

type VideoSnippet struct {
	Title       string `json:"title,omitempty"`
	Description string `json:"description,omitempty"`
}

type VideoStatus struct {
	Visibility string `json:"privacyStatus,omitempty"`
}

type Video struct {
	Snippet VideoSnippet `json:"snippet,omitempty"`
	Status VideoStatus `json:"status,omitempty"`
}

func main() {
	token_file, err := os.Open("./token.secret")
	if err != nil {
		log.Fatalln("No token found!")
		return
	}
	defer token_file.Close()

	raw_token, err := ioutil.ReadAll(token_file)
	if err != nil {
		log.Fatalln("Cannot read contents")
		return
	}
	token := string(raw_token)

	instance := Video{
		Snippet: &VideoSnippet{
			Title: "lali nakku enna pakada",
			Description: "nakku nu sonnen",
		},
	}

	video_file, err := os.Open("./summa.mp4")
	if err != nil {
		log.Fatalln("Cannot open video", err.Error())
		return
	}
	defer video_file.Close()

	post_body := &bytes.Buffer{}
	pencil := multipart.NewWriter(post_body)
	video, err := pencil.CreateFormFile("media", "summa.mp4")
	if err != nil {
		log.Fatalln(err.Error())
		return
	}
	_, err = io.Copy(video, video_file)
	if err != nil {
		log.Fatalln(err.Error())
		return
	}
	err := pencil.WriteField("snippet",)
}
