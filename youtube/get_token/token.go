package main

import (
	"encoding/json"
	"log"
	"net/http"
	"net/url"
	"os"
)

const (
	OAUTH_URL = "https://accounts.google.com/o/oauth2/auth"
	TOKEN_URL = "https://accounts.google.com/o/oauth2/token"
	REDIRECT_URI = "http://localhost:3000/oauth"
)

type Scope []string

func GetScopes(scope *Scope) string {
	var val string
	for _, s := range *scope {
		val += s
		val += " "
	}
	return val
}

type OauthResponse struct {
	Token string `json:"access_token"`
}


var client_id, client_secret string
var upload_scope Scope
var oauth_resp OauthResponse

func init() {
	var ok bool
	client_id, ok = os.LookupEnv("YT_CLIENT_ID")
	if !ok {
		log.Fatalln("Failed to obtain client id...")
		return
	}
	client_secret, ok = os.LookupEnv("YT_CLIENT_SECRET")
	if !ok {
		log.Fatalln("Failed to obtain client secret...")
	}

	upload_scope = []string{"https://www.googleapis.com/auth/youtube.upload", "https://www.googleapis.com/auth/youtube"}
	GetScopes(&upload_scope)
}

func oauth_flow() {
	params := url.Values{}
	params.Set("response_type", "code")
	params.Set("client_id", client_id)
	params.Set("redirect_uri", REDIRECT_URI)
	params.Set("scope", GetScopes(&upload_scope))

	log.Println("Enter this website to obtain your token!")
	log.Println(OAUTH_URL + "?" + params.Encode())
}

func HandleOauth(w http.ResponseWriter, r *http.Request) {
	code := r.URL.Query().Get("code")
	params := url.Values{}
	params.Set("code", code)
	params.Set("client_id", client_id)
	// Why the fuck do i need to send this over a fucking url?
	params.Set("client_secret", client_secret)
	params.Set("redirect_uri", REDIRECT_URI)
	params.Set("grant_type", "authorization_code")
	res, err := http.Post(TOKEN_URL + "?" + params.Encode(), "", nil)
	if err != nil {
		log.Fatalln("Failed to send request!")
		os.Exit(1)
	}
	defer res.Body.Close()

	err = json.NewDecoder(res.Body).Decode(&oauth_resp)
	if err != nil {
		log.Fatalln("Oopsie, something went wrong!")
		os.Exit(1)
	}

	file, err := os.Create("token.secret")
	if err != nil {
		log.Fatalln("Failed creating file")
		os.Exit(1)
	}
	log.Println(oauth_resp.Token)
	_, err = file.WriteString(oauth_resp.Token)
	if err != nil {
		log.Fatalln("Failed writing to file!")
		os.Exit(1)
	}
	os.Exit(0)
}

func main() {
	oauth_flow()
	http.HandleFunc("/oauth", HandleOauth)
	log.Fatal(http.ListenAndServe(":3000", nil))
}
